<?php

namespace App\Services;

use Exception;
use RuntimeException;
use Symfony\Bridge\ProxyManager\LazyProxy\Instantiator\RuntimeInstantiator;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Console\Application as BaseApplication;
use Symfony\Component\Console\CommandLoader\ContainerCommandLoader;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Throwable;

/**
 * Class Application
 *
 * /src/Composer/Console/Application.php
 *
 * @package App\Services
 */
class Application extends BaseApplication
{
    private ContainerBuilder $container;
    public  string           $appDirectory;
    public  string           $cacheDirectory;
    public  string           $viewsDirectory;

    /**
     * @throws Exception
     */
    public function __construct(string $appDirectory)
    {
        parent::__construct('Моя програмка');

        $this->appDirectory = $appDirectory;
        $this->cacheDirectory = $appDirectory . '/cache';
        $this->viewsDirectory = $appDirectory . '/views';

        $this->container = $this->createDIContainer();
        $this->loadCommands($this->container);
    }

    public function getContainer(): Container
    {
        return $this->container;
    }

    /**
     * Добавление новых глобальных опций
     *
     * @return InputDefinition
     */
    protected function getDefaultInputDefinition(): InputDefinition
    {
        $definition = parent::getDefaultInputDefinition();
        $definition->addOption(new InputOption(
            'working-dir',
            'd',
            InputOption::VALUE_REQUIRED,
            'If specified, use the given directory as working directory'
        ));

        return $definition;
    }

    /**
     * Смена стартовой директории при указании опции
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws Throwable
     */
    public function doRun(InputInterface $input, OutputInterface $output): int
    {
        $workingDir = $this->getNewWorkingDirectory($input);

        if (!empty($workingDir)) {
            chdir($workingDir);
        }

        return parent::doRun($input, $output);
    }

    private function getNewWorkingDirectory(InputInterface $input)
    {
        $directory = $input->getParameterOption(['--working-dir', '-d']) ?: getcwd();
        if ($directory !== false && !is_dir($directory)) {
            throw new RuntimeException(sprintf('Invalid working directory specified, %s does not exist.', $directory));
        }

        return $directory;
    }

    /**
     * Инициализация контейнера, загрузка команд
     *
     * @throws Exception
     */
    private function createDIContainer(): ContainerBuilder
    {
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->setProxyInstantiator(new RuntimeInstantiator());

        $loader = new YamlFileLoader($containerBuilder, new FileLocator('config'));
        $loader->load($this->appDirectory . '/config/services.yaml');
        $loader->load($this->appDirectory . '/config/commands.yaml');

        $containerBuilder->compile();
        $containerBuilder->set(self::class, $this);

        return $containerBuilder;
    }

    /**
     * Регистрация специально помеченных сервисов как команды утилиты
     *
     * @param ContainerBuilder $containerBuilder
     */
    private function loadCommands(ContainerBuilder $containerBuilder)
    {
        $services = $containerBuilder->findTaggedServiceIds('console.command');
        $commandMap = [];
        foreach ($services as $service => $tags) {
            foreach ($tags as $tag) {
                $commandMap[$tag['command']] = $service;
            }
        }
        $this->setCommandLoader(new ContainerCommandLoader($containerBuilder, $commandMap));
    }
}
