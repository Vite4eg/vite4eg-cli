<?php

namespace App\Services;

use Bitrix\Main\Application;
use Bitrix\Main\Data\Connection;

/**
 * Class Bitrix
 *
 * Основная идея сервиса - инициализация ядра битрикс
 *
 * @package App
 */
class Bitrix
{
    private string      $docroot;
    public  Application $application;
    public  Connection  $connection;

    public function __construct(?string $path)
    {
        define('NO_KEEP_STATISTIC', true);
        define('NOT_CHECK_PERMISSIONS', true);

        $this->docroot = $path ?: getcwd();

        $_SERVER['DOCUMENT_ROOT'] = $this->docroot;
        $prologBefore = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';
        if (!is_readable($prologBefore)) {
            throw new \RuntimeException(sprintf('Bitrix prolog file [%s] is not available', $prologBefore));
        }
        require_once $prologBefore;

        $this->application = Application::getInstance();
        $this->connection = Application::getConnection();
    }

    public function getDocroot()
    {
        return $this->docroot;
    }
}
