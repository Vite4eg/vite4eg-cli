<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class BitrixComponentCommand extends Command
{
    protected static $defaultName = 'bitrix:component';

    protected Environment $twig;
    protected Filesystem  $fileSystem;

    public function __construct(Environment $twig, Filesystem $fileSystem)
    {
        parent::__construct(self::$defaultName);

        $this->twig = $twig;
        $this->fileSystem = $fileSystem;
    }

    protected function configure()
    {
        $this
            ->setDescription('Создание битрикс-компонента')
            ->setDescription('Создаёт заготовку компонента битрикс')
            ->addArgument('name', InputArgument::REQUIRED, 'Имя компонента')
            ->addUsage('-C /var/www/my-site/www/local/components vendor:news.list')
            ->addUsage('vendor:news.list --prepare --no-template')
        ;

        $this->addOption(
            'prepare',
            '',
            InputOption::VALUE_NONE,
            'Сгенерировать для компонента метод prepareParams()'
        );

        $this->addOption(
            'no-template',
            '',
            InputOption::VALUE_NONE,
            'Не генерировать шаблон компонента'
        );

        $this->addOption(
            'path',
            '-C',
            InputOption::VALUE_REQUIRED,
            'Запустить команду в каталоге <path>'
        );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $name = $input->getArgument('name');
        $prepare = $input->getOption('prepare');
        $path = $input->getOption('path') ?? getcwd();
        $noTemplate = $input->getOption('no-template');

        $componentDir = $this->getComponentDir($path, $name);
        $className = $this->getComponentClassName($name);

        $this->fileSystem->mkdir($componentDir);
        $this->fileSystem->appendToFile(
            $componentDir. '/class.php',
            $this->twig->render('bitrix.component.php.twig', [
                'name'    => $className,
                'prepare' => $prepare,
            ])
        );

        if ($noTemplate !== true) {
            $this->fileSystem->mkdir($componentDir . '/templates/.default/');
            $this->fileSystem->appendToFile(
                $componentDir. '/templates/.default/template.php',
                $this->twig->render('bitrix.component.template.php.twig')
            );
        }

        $output->writeln("Компонент создан: $componentDir");

        return 0;
    }

    /**
     * @param string $path
     * @param string $name
     * @return string
     */
    protected function getComponentDir(string $path, string $name): string
    {
        $realPath = realpath($path);
        if ($realPath === false) {
            $message = sprintf('Path "%s" could not be found.', $path);
            throw new IOException($message);
        }

        return sprintf('%s/%s', $realPath, str_replace(':', DIRECTORY_SEPARATOR, $name));
    }

    /**
     * @param string $componentName
     * @return string
     */
    protected function getComponentClassName(string $componentName): string
    {
        $bxComponent = explode(':', $componentName);
        $componentName = end($bxComponent);

        return sprintf('%sComponent', str_replace('.', '', ucwords($componentName, '.')));
    }
}
