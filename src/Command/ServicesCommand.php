<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ServicesCommand extends Command
{
    protected static $defaultName = 'services';

    protected function configure()
    {
        $this
            ->setDescription('Список сервисов')
            ->setDescription('Выводит список зарегистрированных сервисов')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln($this->getApplication()->getContainer()->getServiceIds());

        return 0;
    }
}
