<?php

namespace App\Command;

use App\Services\Bitrix;
use Faker\Generator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HelloCommand extends Command
{
    protected static $defaultName = 'hello';

    protected Bitrix    $bitrix;
    protected Generator $faker;

    public function __construct(Bitrix $bitrix, Generator $faker)
    {
        parent::__construct(self::$defaultName);

        $this->bitrix = $bitrix;
        $this->faker = $faker;
    }

    protected function configure()
    {
        $this->setDescription('Приветственная команда');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Hello World!');
        $output->writeln('Bitrix Document Root: ' . $this->bitrix->getDocroot());
        $output->writeln('Random name from Faker: ' . $this->faker->name());
        $output->writeln($input->getParameterOption('working-dir'));

        return 0;
    }
}
