Вспомогательная утилита для внутренних нужд.

Основана на [Symfony Console](https://symfony.com/doc/current/components/console.html) + [Symfony DI Component](https://symfony.com/doc/current/components/dependency_injection.html).

Данная утилита может использовать API битрикс. Чтоб использовать битровое API, достаточно подключить файл пролога.
Для этого организован сервис **Bitrix**: его основная задача - это инициализация ядра.
Сервис прописан в DI контейнер, так что он автовайрится при указании в конструкторе:

```php
use App\Services\Bitrix;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExampleCommand extends Command
{
    protected static $defaultName = 'example';

    protected Bitrix $bitrix;

    public function __construct(Bitrix $bitrix)
    {
        parent::__construct(self::$defaultName);
        $this->bitrix = $bitrix;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // в конструкторе сервиса $bitrix подключается пролог
        // можно использовать API битрикс
        $serverName = \Bitrix\Main\Config\Option::get('main', 'server_name');

        $output->writeln('Example');
        $output->writeln('Bitrix Document Root: ' . $this->bitrix->getDocroot());
        return 0;
    }
}
```

После этого в командах можно использовать битровый функционал.

Для того чтоб битровый пролог мог подключиться, утилиту надо запустить либо в каталоге `$_SERVER['DOCUMENT_ROOT']`, либо указать параметр `-d`
```bash
./tool.php -d /var/www/my-site/www command
```

Так же в проекте используется прокси пакет, так что ядро битрикс инициализируется только в тот момент, когда к нему идёт обращение.
То есть команда `list` не вызовет инициализацию сервиса Bitrix



## Конфигурация
* `config/services.yaml` — файл для регистрации различных сервисов. Для этих сервисов работает autowire
* `config/commands.yaml` — файл для регистрации команд утилиты

  ```yaml
  services:

    App\Command\BitrixComponentCommand: # класс, отвечающий за команду
      tags: [{ name: 'console.command', command: 'bitrix:component' }]

    App\Command\BitrixComponentCommand: # или можно так
      tags:
        - { name: 'console.command', command: 'bitrix:component' }

    App\Command\BitrixComponentCommand: # ещё один вариант
      tags:
        - name: 'console.command'
          command: 'bitrix:component'
  ```
* `App\Command\BitrixComponentCommand` — класс, отвечающий за команду
* `tags` — теги для настройки приложения
  * `name` — `console.command`. По этому имени приложение регистрирует сервис как команду для тулзы
  * `command` — название команды



## Сервисы
* Bitrix — отвечает за подключение пролога Битрикс
* [Faker](https://fakerphp.github.io/) — библиотека для генерации фейковых данных
* [Twig](https://twig.symfony.com/) — шаблонизатор для php. Можно использовать для кодогенерации
* [Filesystem](https://symfony.com/doc/current/components/filesystem.html) — библиотека для работы с файловой системой



## Автовайринг
Сервисы регистрируются с параметром `autowire: true`. Так что работает автоинжект:
```php
use App\Services\Bitrix;
use Faker\Generator;
use Symfony\Component\Console\Command\Command;

class ExampleCommand extends Command
{
    protected static $defaultName = 'example';

    protected Bitrix    $bitrix;
    protected Generator $faker;

    public function __construct(Bitrix $bitrix, Generator $faker)
    {
        parent::__construct(self::$defaultName);

        $this->bitrix = $bitrix;
        $this->faker = $faker;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Hello World!');
        $output->writeln('Bitrix Document Root: ' . $this->bitrix->getDocroot());
        $output->writeln('Random name from Faker: ' . $this->faker->name());
        return 0;
    }
}
```



## Шаблонизация
Для шаблонизации используется **Twig**. Шаблоны располагаются в папке **views**.
Для использования зарегистрирован сервис `Twig\Environment`:

```php
use Symfony\Component\Console\Command\Command;
use Twig\Environment;

class ExampleCommand extends Command
{
    protected static $defaultName = 'example';
    protected Environment $twig;

    public function __construct(Environment $twig)
    {
        parent::__construct(self::$defaultName);
        $this->twig = $twig;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // шаблон: views/example.php.twig
        $content = $this->twig->render('example.php.twig');
        $output->writeln($content);
        return 0;
    }
}
```
