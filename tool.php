#!/usr/bin/env php
<?php

use App\Services\Application;

require_once 'vendor/autoload.php';

(new Application(__DIR__))->run();
